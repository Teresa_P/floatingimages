# README #

This prototype was kindly developed for Professor Robert Spence, Imperial College, UK  

It requires Unity 5.5, download here: https://unity3d.com/pt/get-unity/download

Executable versions can be found here: 

(Windows) [https://bitbucket.org/Teresa_P/floatingimages/downloads/FloatingImagesForWindows.rar](https://bitbucket.org/Teresa_P/floatingimages/downloads/FloatingImagesForWindows.rar) 

(Mac)  
 

 
Application accepts any .jpg or .png image. Add your own images to the folder images located under the application folder: 
  
(Windows) FloatingImages_Data/StreamingAssets/Images/ 
 
(Mac) Contents/Resources/Data/StreamingAssets/Images/ 

### Floating Images project files ###

* Open FloatingImages Scene that can be found inside Assets Folder
* Press CTRL+R to run the application, or press "Run"
* In runtime press "M" to toggle setings menu and "A" to toggle the axis visibility