﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadImages : MonoBehaviour {

    public static List<Sprite> ImagesList = new List<Sprite>();


    void Start ()
    {
        LoadImagesToList();	
	}

    private static void LoadImagesToList()
    {
        var di = new DirectoryInfo(Application.streamingAssetsPath + @"/Images/");
        
        var smFiles = di.GetFiles();
        var i = 0;

        foreach (FileInfo fi in smFiles)
        {
            //exclude .meta files
            if (fi.Extension == ".jpg" || fi.Extension == ".png" || fi.Extension == ".JPG" || fi.Extension == ".PNG")
            {
                {
                    WWW www = new WWW("file://" + Application.streamingAssetsPath + @"/Images/" + fi.Name);
                    
                    var tempText = www.texture;
                    var tempSprite = Sprite.Create(tempText, new Rect(0, 0, tempText.width, tempText.height),
                        new Vector2(0.5f, 0.5f));
                    ImagesList.Add(tempSprite);
                    i++;
                }
            }
        }
    }
}
