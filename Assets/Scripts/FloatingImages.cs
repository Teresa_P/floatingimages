﻿using UnityEngine;
using UnityEngine.UI;

public class FloatingImages : MonoBehaviour {

    public Transform TopTarget, RightTarget, BottomTarget, LeftTarget;
    public GameObject TopImage, RightImage, BottomImage, LeftImage, Menu, Axis;
    public GameObject TopPanel, TopRightPanel, RightPanel, BottomRightPanel, BottomPanel, BottomLeftPanel, LeftPanel, TopLeftPanel;
    public Text MinScaleText, ScaleFactorText, ImagesPerScdText, SpeedText, OffSetText;
    private float _instantiationTimer = 1, _individualTimer;
    private int _imagesPerSecond = 3, _imgIndex = 0, _sIndex;
    private float _speed = 120.0f, _minScale = 0.1f, _scaleFactor = 0.3f, _step, _offSet;
    private bool _imagesAsCircles, _spwanAll = true;

    private int _counter = 1;
    
    private void Start()
    {
        SetTargetsPosition();
        Menu.SetActive(false);
        Axis.SetActive(false);
    }
    
    public void SetMinScale(float sc)
    {
        _minScale = sc;
        MinScaleText.text = _minScale.ToString("0.0");
    }
 
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M)) Menu.SetActive(!Menu.activeSelf);
        if (Input.GetKeyDown(KeyCode.A)) Axis.SetActive(!Axis.activeSelf);
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

        if (_spwanAll)
            _instantiationTimer -= Time.deltaTime;
        else
            _individualTimer -= Time.deltaTime;

        if (_instantiationTimer < 0)
        {
            SpawnTopImages();
            SpawnTopRightImages();
            SpawnRightImages();
            SpawnBottomRightImages();
            SpawnBottomImages();
            SpawnBottomLeftImages();
            SpawnLeftImages();
            SpawnTopLeftImages();
            _instantiationTimer = (float) 1 / _imagesPerSecond;
        }
        
        if(_individualTimer < 0)
        {
            switch (_counter)
            {
                case 1:
                    SpawnTopImages();
                    break;
                case 2:
                    SpawnTopRightImages();
                    break;
                case 3:
                    SpawnRightImages();
                    break;
                case 4:
                    SpawnBottomRightImages();
                    break;
                case 5:
                    SpawnBottomImages();
                    break;
                case 6:
                    SpawnBottomLeftImages();
                    break;
                case 7:
                    SpawnLeftImages();
                    break;
                case 8:
                    SpawnTopLeftImages();
                    break;
            }
            _individualTimer = (float) 1 / _imagesPerSecond /8;
        }
        _step = _speed * Time.deltaTime;

        ImagesBehavior();
    }
    
    /// <summary>
    /// Setting targets position. These targets indicate the position where images should be destroyed
    /// </summary>
    private void SetTargetsPosition()
    {
        var rightParent = RightTarget.transform.parent.GetComponent<RectTransform>().rect.width;
        var rightWidth = RightTarget.gameObject.GetComponent<RectTransform>().sizeDelta.x;
        RightTarget.position = new Vector2(2 * rightParent + rightWidth, RightTarget.transform.position.y);

        var bottomParent = BottomTarget.transform.parent.GetComponent<RectTransform>().rect.height;
        var bottomHeight = BottomTarget.gameObject.GetComponent<RectTransform>().sizeDelta.y;
        BottomTarget.position = new Vector2(BottomTarget.transform.position.x, - bottomHeight);

        var leftParent = LeftTarget.transform.parent.GetComponent<RectTransform>().rect.width;
        var leftWidth = LeftTarget.gameObject.GetComponent<RectTransform>().sizeDelta.x;
        LeftTarget.position = new Vector2(- leftWidth, LeftTarget.transform.position.y);

        var topParent = TopTarget.transform.parent.GetComponent<RectTransform>().rect.height;
        var topHeight = TopTarget.gameObject.GetComponent<RectTransform>().sizeDelta.y;
        TopTarget.position = new Vector2(TopTarget.transform.position.x, 2 * topParent + topHeight);
    }

    /// <summary>
    /// Instatiation of images
    /// </summary>
    private void SpawnTopImages()
    {
        if (TopPanel.activeSelf)
        {
            var newTopImage = Instantiate(TopImage);
            newTopImage.transform.SetParent(GameObject.Find("TopPanel").transform);
            newTopImage.transform.SetAsFirstSibling();
            newTopImage.transform.localScale = new Vector3(_minScale, _minScale);
            newTopImage.transform.position = new Vector2(newTopImage.transform.parent.position.x,
                newTopImage.transform.parent.GetComponent<RectTransform>().rect.height + _offSet);
            newTopImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newTopImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float)LoadImages.ImagesList[_imgIndex].texture.width / LoadImages.ImagesList[_imgIndex].texture.height;
            if (_imagesAsCircles)
            {
                newTopImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newTopImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                newTopImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                    (float)LoadImages.ImagesList[_imgIndex].texture.width /
                    LoadImages.ImagesList[_imgIndex].texture.height;
                newTopImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
            }
            else
            {
                newTopImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newTopImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
        }
        _counter = 2;
    }

    private void SpawnTopRightImages()
    {
        if (TopRightPanel.activeSelf)
        {
            var newTopRightImage = Instantiate(TopImage);
            newTopRightImage.tag = "TopRightImage";
            newTopRightImage.transform.SetParent(TopRightPanel.transform);
            newTopRightImage.transform.SetAsFirstSibling();
            newTopRightImage.transform.localScale = new Vector3(_minScale, _minScale);
            newTopRightImage.transform.position = new Vector2(TopRightPanel.transform.position.x  -  (TopRightPanel.GetComponent<RectTransform>().rect.width * 0.5f ) + (_offSet * 0.5f) ,
                TopRightPanel.transform.position.y - (TopRightPanel.GetComponent<RectTransform>().rect.height * 0.5f) + (_offSet * 0.5f) );
            newTopRightImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newTopRightImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float)LoadImages.ImagesList[_imgIndex].texture.width / LoadImages.ImagesList[_imgIndex].texture.height;
            if (_imagesAsCircles)
            {
                newTopRightImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newTopRightImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                newTopRightImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                    (float)LoadImages.ImagesList[_imgIndex].texture.width /
                    LoadImages.ImagesList[_imgIndex].texture.height;
                newTopRightImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
            }
            else
            {
                newTopRightImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newTopRightImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
        }
        _counter = 3;
    }

    private void SpawnRightImages()
    {
        if (RightPanel.activeSelf)
        {
            var newRightImage = Instantiate(RightImage);
            newRightImage.transform.SetParent(GameObject.Find("RightPanel").transform);
            newRightImage.transform.SetAsFirstSibling();
            newRightImage.transform.localScale = new Vector3(_minScale, _minScale);
            
            newRightImage.transform.position =
                new Vector2(RightPanel.transform.position.x - (newRightImage.transform.parent.GetComponent<RectTransform>().rect.width * 0.5f) + _offSet,
                    newRightImage.transform.parent.position.y);
            newRightImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newRightImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float) LoadImages.ImagesList[_imgIndex].texture.width/LoadImages.ImagesList[_imgIndex].texture.height;
            if (_imagesAsCircles)
            {
                newRightImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newRightImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                newRightImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                    (float) LoadImages.ImagesList[_imgIndex].texture.width/
                    LoadImages.ImagesList[_imgIndex].texture.height;
                newRightImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
            }
            else
            {
                newRightImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newRightImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
        }
        _counter = 4;
    }

    private void SpawnBottomRightImages()
    {
        if (BottomRightPanel.activeSelf)
        {
            var newBottomRightImage = Instantiate(BottomImage);
            newBottomRightImage.tag = "BottomRightImage";
            newBottomRightImage.transform.SetParent(BottomRightPanel.transform);
            newBottomRightImage.transform.SetAsFirstSibling();
            newBottomRightImage.transform.localScale = new Vector3(_minScale, _minScale);
            newBottomRightImage.transform.position = new Vector2(BottomRightPanel.transform.position.x - (BottomRightPanel.GetComponent<RectTransform>().rect.width * 0.5f) + (_offSet * 0.5f),
            BottomRightPanel.transform.position.y + (TopRightPanel.GetComponent<RectTransform>().rect.height * 0.5f) - (_offSet * 0.5f));
            newBottomRightImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = LoadImages.ImagesList[_imgIndex];
            newBottomRightImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float)LoadImages.ImagesList[_imgIndex].texture.width / LoadImages.ImagesList[_imgIndex].texture.height;
            if (_imagesAsCircles)
            {
                newBottomRightImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newBottomRightImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                newBottomRightImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                    (float)LoadImages.ImagesList[_imgIndex].texture.width /
                    LoadImages.ImagesList[_imgIndex].texture.height;
                newBottomRightImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
                newBottomRightImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
            }
            else
            {
                newBottomRightImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newBottomRightImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;

        }
        _counter = 5;
    }

    private void SpawnBottomImages()
    {
        if (BottomPanel.activeSelf)
        {
            var newBottomImage = Instantiate(BottomImage);
            newBottomImage.transform.SetParent(GameObject.Find("BottomPanel").transform);
            newBottomImage.transform.SetAsFirstSibling();
            newBottomImage.transform.localScale = new Vector3(_minScale, _minScale);
            newBottomImage.transform.position = new Vector2(newBottomImage.transform.parent.position.x,
                newBottomImage.transform.parent.GetComponent<RectTransform>().rect.height - _offSet);
            newBottomImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newBottomImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float) LoadImages.ImagesList[_imgIndex].texture.width/LoadImages.ImagesList[_imgIndex].texture.height;
            if (_imagesAsCircles)
            {
                newBottomImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newBottomImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                newBottomImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                    (float) LoadImages.ImagesList[_imgIndex].texture.width/
                    LoadImages.ImagesList[_imgIndex].texture.height;
                newBottomImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
                newBottomImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
            }
            else
            {
                newBottomImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newBottomImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
            
        }
        _counter = 6;
    }

    private void SpawnBottomLeftImages()
    {
        if (BottomLeftPanel.activeSelf)
        {
            var newBottomLeftImage = Instantiate(LeftImage);
            newBottomLeftImage.tag = "BottomLeftImage";
            newBottomLeftImage.transform.SetParent(BottomLeftPanel.transform);
            newBottomLeftImage.transform.SetAsFirstSibling();
            newBottomLeftImage.transform.localScale = new Vector3(_minScale, _minScale);

            newBottomLeftImage.transform.position = new Vector2(BottomLeftPanel.transform.position.x + (BottomLeftPanel.GetComponent<RectTransform>().rect.width * 0.5f) - (_offSet * 0.5f),
            BottomRightPanel.transform.position.y + (TopRightPanel.GetComponent<RectTransform>().rect.height * 0.5f) - (_offSet * 0.5f));

            //newBottomLeftImage.transform.position =
            //  new Vector2(newBottomLeftImage.transform.parent.GetComponent<RectTransform>().rect.width - _offSet,
            //    newBottomLeftImage.transform.parent.position.y);
            newBottomLeftImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float)LoadImages.ImagesList[_imgIndex].texture.width / LoadImages.ImagesList[_imgIndex].texture.height;
            newBottomLeftImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newBottomLeftImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float)LoadImages.ImagesList[_imgIndex].texture.width / LoadImages.ImagesList[_imgIndex].texture.height;
            newBottomLeftImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,
                0, 0);
            if (_imagesAsCircles)
            {
                newBottomLeftImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newBottomLeftImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }
            else
            {
                newBottomLeftImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newBottomLeftImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
        }
        _counter = 7;
    }

    private void SpawnLeftImages()
    {
        if (LeftPanel.activeSelf)
        {
            var newBottomLeftImage = Instantiate(LeftImage);
            newBottomLeftImage.transform.SetParent(GameObject.Find("LeftPanel").transform);
            newBottomLeftImage.transform.SetAsFirstSibling();
            newBottomLeftImage.transform.localScale = new Vector3(_minScale, _minScale);
            newBottomLeftImage.transform.position =
                new Vector2(newBottomLeftImage.transform.parent.GetComponent<RectTransform>().rect.width - _offSet,
                    newBottomLeftImage.transform.parent.position.y);
            newBottomLeftImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float) LoadImages.ImagesList[_imgIndex].texture.width/LoadImages.ImagesList[_imgIndex].texture.height;
            newBottomLeftImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newBottomLeftImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float) LoadImages.ImagesList[_imgIndex].texture.width/LoadImages.ImagesList[_imgIndex].texture.height;
            newBottomLeftImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,
                0, 0);
            if (_imagesAsCircles)
            {
                newBottomLeftImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newBottomLeftImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }
            else
            {
                newBottomLeftImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newBottomLeftImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
        }
        _counter = 8;
    }

    private void SpawnTopLeftImages()
    {
        if (TopLeftPanel.activeSelf)
        {
            var newTopLeftImage = Instantiate(TopImage);
            newTopLeftImage.tag = "TopLeftImage";
            newTopLeftImage.transform.SetParent(TopLeftPanel.transform);
            newTopLeftImage.transform.SetAsFirstSibling();
            newTopLeftImage.transform.localScale = new Vector3(_minScale, _minScale);
            newTopLeftImage.transform.position = new Vector2(TopLeftPanel.transform.position.x + (TopLeftPanel.GetComponent<RectTransform>().rect.width * 0.5f) - (_offSet * 0.5f),
                TopLeftPanel.transform.position.y - (TopLeftPanel.GetComponent<RectTransform>().rect.height * 0.5f) + (_offSet * 0.5f));
            newTopLeftImage.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =
                LoadImages.ImagesList[_imgIndex];
            newTopLeftImage.transform.GetChild(0).GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                (float)LoadImages.ImagesList[_imgIndex].texture.width / LoadImages.ImagesList[_imgIndex].texture.height;
            if (_imagesAsCircles)
            {
                newTopLeftImage.transform.GetChild(0).GetComponent<Mask>().enabled = true;
                newTopLeftImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                newTopLeftImage.transform.GetChild(0).GetComponent<AspectRatioFitter>().aspectRatio =
                    (float)LoadImages.ImagesList[_imgIndex].texture.width /
                    LoadImages.ImagesList[_imgIndex].texture.height;
                newTopLeftImage.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition =
                    new Vector3(0, 0, 0);
            }
            else
            {
                newTopLeftImage.transform.GetChild(0).GetComponent<Mask>().enabled = false;
                newTopLeftImage.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            _imgIndex++;
            if (_imgIndex >= LoadImages.ImagesList.Count) _imgIndex = 0;
        }
        _counter = 1;
    }



    /// <summary>
    /// Adding movement to images + scaling
    /// </summary>
    private void ImagesBehavior()
    {
        var rightImages = GameObject.FindGameObjectsWithTag("RightImage");
        foreach (var img in rightImages)
        {
            img.transform.Translate(Vector2.right * _step);
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.x > RightTarget.position.x)
                Destroy(img);
        }

        var bottomImages = GameObject.FindGameObjectsWithTag("BottomImage");
        foreach (var img in bottomImages)
        {
            img.transform.Translate(Vector2.down * _step);
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.y < BottomTarget.position.y)
                Destroy(img);
        }

        var leftImages = GameObject.FindGameObjectsWithTag("LeftImage");
        foreach (var img in leftImages)
        {
            img.transform.Translate(Vector2.left * _step);
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.x < LeftTarget.position.x)
                Destroy(img);
        }

        var topImages = GameObject.FindGameObjectsWithTag("TopImage");
        foreach (var img in topImages)
        {
            img.transform.Translate(Vector2.up * _step);
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.y > TopTarget.position.y)
                Destroy(img);
        }

        var topRightImages = GameObject.FindGameObjectsWithTag("TopRightImage");
        foreach (var img in topRightImages)
        {
            img.transform.Translate(Vector2.up * _step);
            img.transform.Translate(Vector2.right * _step);
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.x > RightTarget.position.x)
                Destroy(img);
        }

        var bottomRightImages = GameObject.FindGameObjectsWithTag("BottomRightImage");
        foreach (var img in bottomRightImages)
        {
            img.transform.Translate(Vector2.down * _step );
            img.transform.Translate(Vector2.right * _step );
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.x > RightTarget.position.x)
                Destroy(img);
        }

        var bottomLeftImages = GameObject.FindGameObjectsWithTag("BottomLeftImage");
        foreach (var img in bottomLeftImages)
        {
            img.transform.Translate(Vector2.down * _step );
            img.transform.Translate(Vector2.left * _step );
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.x < LeftTarget.position.x)
                Destroy(img);
        }

        var topLeftImages = GameObject.FindGameObjectsWithTag("TopLeftImage");
        foreach (var img in topLeftImages)
        {
            img.transform.Translate(Vector2.up * _step );
            img.transform.Translate(Vector2.left * _step);
            img.transform.localScale += new Vector3(_scaleFactor, _scaleFactor, 0) * Time.deltaTime;
            if (img.transform.position.x < LeftTarget.position.x)
                Destroy(img);
        }
    }
    
    /// <summary>
    /// The following functions are invoked from the interface sliders
    /// </summary>
   
    public void SetSpeed(float sp)
    {
        _speed = sp;
        SpeedText.text = _speed.ToString("0");
    }

    public void SetScaleFactor(float sf)
    {
        _scaleFactor = sf;
        ScaleFactorText.text = _scaleFactor.ToString("0.0");
    }

    public void SetOffSet(float os)
    {
        _offSet = os;
        OffSetText.text = _offSet.ToString("0.0");
    }

    public void SetImagesPerSecond(float ips)
    {
        _imagesPerSecond = (int)ips;
        ImagesPerScdText.text = _imagesPerSecond.ToString();
    }

    public void SetImagesAsCircles(bool circle)
    {
        _imagesAsCircles = circle;
    }

    public void SetSpawnAll(bool all)
    {
        _spwanAll = all;
        _individualTimer = (float)1 / _imagesPerSecond * 0.25f;
        _individualTimer = (float)1 / _imagesPerSecond * 0.25f;
    }
}
